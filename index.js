// alert("hi")


function Pokemon(name, level) {
	// properties
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;

	// methods
	this.tackle = function(target) {
		target.health -= this.attack
		if(target.health >= 5) {
			console.log(`${this.name} tackled ${target.name}.`)
			console.log(`${target.name}'s health is now reduced to ${target.health}.`)
		} else {
			console.log(`${this.name} tackled ${target.name}.`)
			console.log(`${target.name}'s health is now reduced to ${target.health}.`)
			target.faint();
		}	
	};

	this.faint = function() {
		console.log(`${this.name} fainted.`)

	}

}

let bulbasaur = new Pokemon(`Bulbasaur`, 30);
let squirtle = new Pokemon(`Squirtle`, 20);

console.log(bulbasaur);
console.log(squirtle);

// squirtle.tackle(bulbasaur);

